import requests
from django.conf import settings


def get_data_from_api(url: str):
    response = requests.get(
        settings.KMIT_CP_API_URL + url,
        params={
            "ApiKey": settings.KMIT_CP_API_KEY,
        },
    )

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    return response.json()


def post_data_to_api(url: str, data: dict):
    response = requests.post(
        settings.KMIT_CP_API_URL + url,
        params={
            "ApiKey": settings.KMIT_CP_API_KEY,
        },
        data=data,
    )

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    return response.json()


def put_data_to_api(url: str, data: dict):
    response = requests.put(
        settings.KMIT_CP_API_URL + url,
        params={
            "ApiKey": settings.KMIT_CP_API_KEY,
        },
        data=data,
    )

    if response.status_code not in (200, 201, 204):
        return False

    if response.status_code == 204:
        return True

    return response.json()
