from django.db import models


class Citrixapplicationstodesktop(models.Model):
    applicationrefdesktopgroupid = models.OneToOneField(
        "CitrixApplications",
        models.DO_NOTHING,
        db_column="ApplicationRefDesktopGroupId",
        primary_key=True,
    )  # Field name made lowercase.
    desktopgrouprefapplicationid = models.ForeignKey(
        "CitrixDesktopgroups",
        models.DO_NOTHING,
        db_column="DesktopGroupRefApplicationId",
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "CitrixApplicationsToDesktop"
        unique_together = (
            ("applicationrefdesktopgroupid", "desktopgrouprefapplicationid"),
        )


class Citrixcompanytodesktopgroup(models.Model):
    companyrefdesktopgroupid = models.OneToOneField(
        "CpCompanies",
        models.DO_NOTHING,
        db_column="CompanyRefDesktopGroupId",
        primary_key=True,
    )  # Field name made lowercase.
    desktopgrouprefcompanyid = models.ForeignKey(
        "CitrixDesktopgroups", models.DO_NOTHING, db_column="DesktopGroupRefCompanyId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "CitrixCompanyToDesktopGroup"
        unique_together = (("companyrefdesktopgroupid", "desktopgrouprefcompanyid"),)


class Citrixusertoapplications(models.Model):
    userrefapplicationid = models.OneToOneField(
        "CpUsers", models.DO_NOTHING, db_column="UserRefApplicationId", primary_key=True
    )  # Field name made lowercase.
    applicationrefuserid = models.ForeignKey(
        "CitrixApplications", models.DO_NOTHING, db_column="ApplicationRefUserId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "CitrixUserToApplications"
        unique_together = (("userrefapplicationid", "applicationrefuserid"),)


class Citrixusertodesktopgroup(models.Model):
    userrefdesktopgroupid = models.OneToOneField(
        "CpUsers",
        models.DO_NOTHING,
        db_column="UserRefDesktopGroupId",
        primary_key=True,
    )  # Field name made lowercase.
    desktopgrouprefuserid = models.ForeignKey(
        "CitrixDesktopgroups", models.DO_NOTHING, db_column="DesktopGroupRefUserId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "CitrixUserToDesktopGroup"
        unique_together = (("userrefdesktopgroupid", "desktopgrouprefuserid"),)


class Migrationhistory(models.Model):
    migrationid = models.CharField(
        db_column="MigrationId", primary_key=True, max_length=150
    )  # Field name made lowercase.
    contextkey = models.CharField(
        db_column="ContextKey", max_length=300
    )  # Field name made lowercase.
    model = models.BinaryField(db_column="Model")  # Field name made lowercase.
    productversion = models.CharField(
        db_column="ProductVersion", max_length=32
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "__MigrationHistory"
        unique_together = (("migrationid", "contextkey"),)


class AdsyncResetpassword(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        "CpCompanies", models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=64
    )  # Field name made lowercase.
    password = models.TextField(db_column="Password")  # Field name made lowercase.
    key = models.TextField(db_column="Key")  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    userid = models.ForeignKey(
        "CpUsers", models.DO_NOTHING, db_column="UserID"
    )  # Field name made lowercase.
    completed = models.BooleanField(db_column="Completed")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "adSync_ResetPassword"


class AdsyncSettings(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        "CpCompanies", models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=255
    )  # Field name made lowercase.
    ad_sync_isenabled = models.BooleanField(
        db_column="AD_SYNC_ISENABLED"
    )  # Field name made lowercase.
    ad_sync_masterid = models.IntegerField(
        db_column="AD_SYNC_MASTERID"
    )  # Field name made lowercase.
    ad_sync_create = models.BooleanField(
        db_column="AD_SYNC_CREATE"
    )  # Field name made lowercase.
    ad_sync_update = models.BooleanField(
        db_column="AD_SYNC_UPDATE"
    )  # Field name made lowercase.
    ad_sync_delete = models.BooleanField(
        db_column="AD_SYNC_DELETE"
    )  # Field name made lowercase.
    ad_sync_displayname = models.BooleanField(
        db_column="AD_SYNC_DISPLAYNAME"
    )  # Field name made lowercase.
    ad_sync_firstname = models.BooleanField(
        db_column="AD_SYNC_FIRSTNAME"
    )  # Field name made lowercase.
    ad_sync_middlename = models.BooleanField(
        db_column="AD_SYNC_MIDDLENAME"
    )  # Field name made lowercase.
    ad_sync_lastname = models.BooleanField(
        db_column="AD_SYNC_LASTNAME"
    )  # Field name made lowercase.
    ad_sync_email = models.BooleanField(
        db_column="AD_SYNC_EMAIL"
    )  # Field name made lowercase.
    ad_sync_department = models.BooleanField(
        db_column="AD_SYNC_DEPARTMENT"
    )  # Field name made lowercase.
    ad_sync_description = models.BooleanField(
        db_column="AD_SYNC_DESCRIPTION"
    )  # Field name made lowercase.
    ad_sync_isuserenabled = models.BooleanField(
        db_column="AD_SYNC_ISUSERENABLED"
    )  # Field name made lowercase.
    ad_sync_passwordneverexpires = models.BooleanField(
        db_column="AD_SYNC_PASSWORDNEVEREXPIRES"
    )  # Field name made lowercase.
    ad_sync_street = models.BooleanField(
        db_column="AD_SYNC_STREET"
    )  # Field name made lowercase.
    ad_sync_city = models.BooleanField(
        db_column="AD_SYNC_CITY"
    )  # Field name made lowercase.
    ad_sync_state = models.BooleanField(
        db_column="AD_SYNC_STATE"
    )  # Field name made lowercase.
    ad_sync_postalcode = models.BooleanField(
        db_column="AD_SYNC_POSTALCODE"
    )  # Field name made lowercase.
    ad_sync_pobox = models.BooleanField(
        db_column="AD_SYNC_POBOX"
    )  # Field name made lowercase.
    ad_sync_country = models.BooleanField(
        db_column="AD_SYNC_COUNTRY"
    )  # Field name made lowercase.
    ad_sync_company = models.BooleanField(
        db_column="AD_SYNC_COMPANY"
    )  # Field name made lowercase.
    ad_sync_jobtitle = models.BooleanField(
        db_column="AD_SYNC_JOBTITLE"
    )  # Field name made lowercase.
    ad_sync_telephonenumber = models.BooleanField(
        db_column="AD_SYNC_TELEPHONENUMBER"
    )  # Field name made lowercase.
    ad_sync_fax = models.BooleanField(
        db_column="AD_SYNC_FAX"
    )  # Field name made lowercase.
    ad_sync_homephone = models.BooleanField(
        db_column="AD_SYNC_HOMEPHONE"
    )  # Field name made lowercase.
    ad_sync_mobilephone = models.BooleanField(
        db_column="AD_SYNC_MOBILEPHONE"
    )  # Field name made lowercase.
    ad_sync_pager = models.BooleanField(
        db_column="AD_SYNC_PAGER"
    )  # Field name made lowercase.
    ad_sync_ipphone = models.BooleanField(
        db_column="AD_SYNC_IPPHONE"
    )  # Field name made lowercase.
    ad_sync_office = models.BooleanField(
        db_column="AD_SYNC_OFFICE"
    )  # Field name made lowercase.
    ad_sync_webpage = models.BooleanField(
        db_column="AD_SYNC_WEBPAGE"
    )  # Field name made lowercase.
    ad_sync_imagefromad = models.BooleanField(
        db_column="AD_SYNC_IMAGEFROMAD"
    )  # Field name made lowercase.
    ad_sync_changepasswordnextlogin = models.BooleanField(
        db_column="AD_SYNC_CHANGEPASSWORDNEXTLOGIN"
    )  # Field name made lowercase.
    lastupdated = models.DateTimeField(
        db_column="LastUpdated"
    )  # Field name made lowercase.
    lastupdatedby = models.TextField(
        db_column="LastUpdatedBy", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "adSync_Settings"


class AdPasswordpolicies(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=255
    )  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=64
    )  # Field name made lowercase.
    distinguishedname = models.TextField(
        db_column="DistinguishedName"
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=256
    )  # Field name made lowercase.
    displayname = models.TextField(
        db_column="DisplayName", blank=True, null=True
    )  # Field name made lowercase.
    precedence = models.IntegerField(
        db_column="Precedence"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ad_PasswordPolicies"


class AdSecuritygroups(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        "CpCompanies", models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36
    )  # Field name made lowercase.
    distinguishedname = models.TextField(
        db_column="DistinguishedName", blank=True, null=True
    )  # Field name made lowercase.
    samaccountname = models.CharField(
        db_column="sAMAccountName", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    created = models.DateTimeField(db_column="Created")  # Field name made lowercase.
    iscpprotected = models.BooleanField(
        db_column="IsCpProtected"
    )  # Field name made lowercase.
    membercount = models.IntegerField(
        db_column="MemberCount"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ad_SecurityGroups"


class AuditLogs(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    action = models.CharField(
        db_column="Action", max_length=12, blank=True, null=True
    )  # Field name made lowercase.
    username = models.CharField(
        db_column="Username", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    ipaddress = models.CharField(
        db_column="IPAddress", max_length=45, blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    targetobject = models.CharField(
        db_column="TargetObject", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    message = models.CharField(
        db_column="Message", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "audit_Logs"


class AuditTraces(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    username = models.TextField(
        db_column="Username", blank=True, null=True
    )  # Field name made lowercase.
    ipaddress = models.TextField(
        db_column="IPAddress", blank=True, null=True
    )  # Field name made lowercase.
    method = models.TextField(
        db_column="Method", blank=True, null=True
    )  # Field name made lowercase.
    route = models.TextField(
        db_column="Route", blank=True, null=True
    )  # Field name made lowercase.
    parameters = models.TextField(
        db_column="Parameters", blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "audit_Traces"


class CitrixApplications(models.Model):
    applicationid = models.AutoField(
        db_column="ApplicationID", primary_key=True
    )  # Field name made lowercase.
    uid = models.IntegerField(db_column="Uid")  # Field name made lowercase.
    uuid = models.CharField(
        db_column="UUID", max_length=36
    )  # Field name made lowercase.
    name = models.TextField(
        db_column="Name", blank=True, null=True
    )  # Field name made lowercase.
    publishedname = models.TextField(
        db_column="PublishedName", blank=True, null=True
    )  # Field name made lowercase.
    applicationname = models.TextField(
        db_column="ApplicationName", blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    securitygroup = models.TextField(
        db_column="SecurityGroup", blank=True, null=True
    )  # Field name made lowercase.
    commandlineexecutable = models.TextField(
        db_column="CommandLineExecutable", blank=True, null=True
    )  # Field name made lowercase.
    commandlinearguments = models.TextField(
        db_column="CommandLineArguments", blank=True, null=True
    )  # Field name made lowercase.
    shortcutaddedtodesktop = models.BooleanField(
        db_column="ShortcutAddedToDesktop"
    )  # Field name made lowercase.
    shortcutaddedtostartmenu = models.BooleanField(
        db_column="ShortcutAddedToStartMenu"
    )  # Field name made lowercase.
    isenabled = models.BooleanField(db_column="IsEnabled")  # Field name made lowercase.
    userfilterenabled = models.BooleanField(
        db_column="UserFilterEnabled"
    )  # Field name made lowercase.
    lastretrieved = models.DateTimeField(
        db_column="LastRetrieved"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "citrix_Applications"


class CitrixDesktopgroups(models.Model):
    desktopgroupid = models.AutoField(
        db_column="DesktopGroupID", primary_key=True
    )  # Field name made lowercase.
    uid = models.IntegerField(db_column="Uid")  # Field name made lowercase.
    uuid = models.CharField(
        db_column="UUID", max_length=36
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    publishedname = models.TextField(
        db_column="PublishedName", blank=True, null=True
    )  # Field name made lowercase.
    securitygroup = models.TextField(
        db_column="SecurityGroup", blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    isenabled = models.BooleanField(db_column="IsEnabled")  # Field name made lowercase.
    lastretrieved = models.DateTimeField(
        db_column="LastRetrieved"
    )  # Field name made lowercase.
    applicationid = models.IntegerField(
        db_column="ApplicationId"
    )  # Field name made lowercase.
    desktopid = models.IntegerField(db_column="DesktopId")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "citrix_DesktopGroups"


class CitrixDesktops(models.Model):
    desktopid = models.AutoField(
        db_column="DesktopID", primary_key=True
    )  # Field name made lowercase.
    uid = models.IntegerField(db_column="Uid")  # Field name made lowercase.
    desktopgroupid = models.ForeignKey(
        CitrixDesktopgroups, models.DO_NOTHING, db_column="DesktopGroupID"
    )  # Field name made lowercase.
    sid = models.TextField(db_column="SID")  # Field name made lowercase.
    agentversion = models.TextField(
        db_column="AgentVersion"
    )  # Field name made lowercase.
    cataloguid = models.IntegerField(
        db_column="CatalogUid"
    )  # Field name made lowercase.
    catalogname = models.TextField(
        db_column="CatalogName", blank=True, null=True
    )  # Field name made lowercase.
    dnsname = models.TextField(
        db_column="DNSName", blank=True, null=True
    )  # Field name made lowercase.
    machinename = models.TextField(
        db_column="MachineName", blank=True, null=True
    )  # Field name made lowercase.
    machineuid = models.IntegerField(
        db_column="MachineUid"
    )  # Field name made lowercase.
    ostype = models.TextField(
        db_column="OSType", blank=True, null=True
    )  # Field name made lowercase.
    osversion = models.TextField(
        db_column="OSVersion", blank=True, null=True
    )  # Field name made lowercase.
    ipaddress = models.TextField(
        db_column="IPAddress", blank=True, null=True
    )  # Field name made lowercase.
    inmaintenancemode = models.BooleanField(
        db_column="InMaintenanceMode"
    )  # Field name made lowercase.
    lastretrieved = models.DateTimeField(
        db_column="LastRetrieved"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "citrix_Desktops"


class CitrixSecuritygroups(models.Model):
    groupid = models.AutoField(
        db_column="GroupID", primary_key=True
    )  # Field name made lowercase.
    groupname = models.TextField(db_column="GroupName")  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    desktopgroupid = models.ForeignKey(
        CitrixDesktopgroups, models.DO_NOTHING, db_column="DesktopGroupID"
    )  # Field name made lowercase.
    applicationid = models.IntegerField(
        db_column="ApplicationID", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "citrix_SecurityGroups"


class CpApikeys(models.Model):
    userid = models.OneToOneField(
        "CpUsers", models.DO_NOTHING, db_column="UserID", primary_key=True
    )  # Field name made lowercase.
    key = models.TextField(
        db_column="Key", blank=True, null=True
    )  # Field name made lowercase.
    ipaddress = models.TextField(
        db_column="IPAddress", blank=True, null=True
    )  # Field name made lowercase.
    superadmin = models.BooleanField(
        db_column="SuperAdmin"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_ApiKeys"


class CpBlockedipaddresses(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    username = models.CharField(
        db_column="Username", max_length=64
    )  # Field name made lowercase.
    ipaddress = models.CharField(
        db_column="IpAddress", max_length=128
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_BlockedIpAddresses"


class CpBrandings(models.Model):
    brandingid = models.AutoField(
        db_column="BrandingID", primary_key=True
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    phone = models.TextField(db_column="Phone")  # Field name made lowercase.
    hostname = models.TextField(db_column="HostName")  # Field name made lowercase.
    email = models.TextField(db_column="Email")  # Field name made lowercase.
    loginlogo = models.TextField(
        db_column="LoginLogo", blank=True, null=True
    )  # Field name made lowercase.
    headerlogo = models.TextField(
        db_column="HeaderLogo", blank=True, null=True
    )  # Field name made lowercase.
    menutype = models.IntegerField(db_column="MenuType")  # Field name made lowercase.
    headerbackgroundcolor = models.TextField(
        db_column="HeaderBackgroundColor", blank=True, null=True
    )  # Field name made lowercase.
    headerforegroundcolor = models.TextField(
        db_column="HeaderForegroundColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebarbackgroundcolor = models.TextField(
        db_column="SidebarBackgroundColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebarbackgroundhovercolor = models.TextField(
        db_column="SidebarBackgroundHoverColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebarbackgroundsubmenucolor = models.TextField(
        db_column="SidebarBackgroundSubMenuColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebarbackgroundsubmenuhovercolor = models.TextField(
        db_column="SidebarBackgroundSubMenuHoverColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebarforegroundcolor = models.TextField(
        db_column="SidebarForegroundColor", blank=True, null=True
    )  # Field name made lowercase.
    sidebariconscolor = models.TextField(
        db_column="SidebarIconsColor", blank=True, null=True
    )  # Field name made lowercase.
    panelheaderbackgroundcolor = models.TextField(
        db_column="PanelHeaderBackgroundColor", blank=True, null=True
    )  # Field name made lowercase.
    panelheaderforegroundcolor = models.TextField(
        db_column="PanelHeaderForegroundColor", blank=True, null=True
    )  # Field name made lowercase.
    headerforegroundhovercolor = models.TextField(
        db_column="HeaderForegroundHoverColor", blank=True, null=True
    )  # Field name made lowercase.
    leftbar_custom_bg_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_link_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_link_hover_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_menu_icon_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_hover_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_hover_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_active_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_active_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_menu_border_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_submenu_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_submenu_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_submenu_hover_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_submenu_hover_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    leftbar_custom_muted_color = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_custom_color1 = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_custom_color2 = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_custom_color3 = models.CharField(max_length=7, blank=True, null=True)
    leftbar_custom_custom_color4 = models.CharField(max_length=7, blank=True, null=True)
    topnav_custom_bg_color = models.CharField(max_length=7, blank=True, null=True)
    topnav_custom_color = models.CharField(max_length=7, blank=True, null=True)
    topnav_custom_link_color = models.CharField(max_length=7, blank=True, null=True)
    topnav_custom_link_hover_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    topnav_custom_link_bg_color = models.CharField(max_length=7, blank=True, null=True)
    topnav_custom_link_hover_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    topnav_custom_link_active_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    topnav_custom_link_active_bg_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    topnav_custom_link_disabled_color = models.CharField(
        max_length=7, blank=True, null=True
    )
    reportlogo = models.TextField(
        db_column="ReportLogo", blank=True, null=True
    )  # Field name made lowercase.
    loginbackgroundcolor = models.TextField(
        db_column="LoginBackgroundColor", blank=True, null=True
    )  # Field name made lowercase.
    loginforegroundcolor = models.TextField(
        db_column="LoginForegroundColor", blank=True, null=True
    )  # Field name made lowercase.
    loginhyperlinkcolor = models.TextField(
        db_column="LoginHyperlinkColor", blank=True, null=True
    )  # Field name made lowercase.
    loginpanelcolor = models.TextField(
        db_column="LoginPanelColor", blank=True, null=True
    )  # Field name made lowercase.
    loginfootercolor = models.TextField(
        db_column="LoginFooterColor", blank=True, null=True
    )  # Field name made lowercase.
    resellercode = models.TextField(
        db_column="ResellerCode", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Brandings"


class CpCompanies(models.Model):
    companyid = models.AutoField(
        db_column="CompanyId", primary_key=True
    )  # Field name made lowercase.
    isreseller = models.BooleanField(
        db_column="IsReseller"
    )  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    orgplanid = models.IntegerField(
        db_column="OrgPlanID", blank=True, null=True
    )  # Field name made lowercase.
    companyname = models.CharField(
        db_column="CompanyName", max_length=256
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    street = models.CharField(
        db_column="Street", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    city = models.CharField(
        db_column="City", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    state = models.CharField(
        db_column="State", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    zipcode = models.CharField(
        db_column="ZipCode", max_length=40, blank=True, null=True
    )  # Field name made lowercase.
    phonenumber = models.CharField(
        db_column="PhoneNumber", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    website = models.CharField(
        db_column="Website", max_length=2048, blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    adminname = models.CharField(
        db_column="AdminName", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    adminemail = models.CharField(
        db_column="AdminEmail", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    distinguishedname = models.CharField(
        db_column="DistinguishedName", max_length=255
    )  # Field name made lowercase.
    created = models.DateTimeField(db_column="Created")  # Field name made lowercase.
    exchenabled = models.BooleanField(
        db_column="ExchEnabled"
    )  # Field name made lowercase.
    lyncenabled = models.BooleanField(
        db_column="LyncEnabled", blank=True, null=True
    )  # Field name made lowercase.
    citrixenabled = models.BooleanField(
        db_column="CitrixEnabled", blank=True, null=True
    )  # Field name made lowercase.
    exchpfplan = models.IntegerField(
        db_column="ExchPFPlan", blank=True, null=True
    )  # Field name made lowercase.
    country = models.CharField(
        db_column="Country", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    exchpermfixed = models.BooleanField(
        db_column="ExchPermFixed", blank=True, null=True
    )  # Field name made lowercase.
    reference = models.CharField(
        db_column="Reference", max_length=128, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Companies"


class CpDomains(models.Model):
    domainid = models.AutoField(
        db_column="DomainID", primary_key=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    domain = models.CharField(
        db_column="Domain", max_length=255
    )  # Field name made lowercase.
    issubdomain = models.BooleanField(
        db_column="IsSubDomain", blank=True, null=True
    )  # Field name made lowercase.
    islyncdomain = models.BooleanField(
        db_column="IsLyncDomain", blank=True, null=True
    )  # Field name made lowercase.
    isdefault = models.BooleanField(db_column="IsDefault")  # Field name made lowercase.
    isaccepteddomain = models.BooleanField(
        db_column="IsAcceptedDomain"
    )  # Field name made lowercase.
    domaintype = models.IntegerField(
        db_column="DomainType", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Domains"


class CpFailedloginattempts(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    username = models.CharField(
        db_column="Username", max_length=64
    )  # Field name made lowercase.
    ipaddress = models.CharField(
        db_column="IpAddress", max_length=128
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_FailedLoginAttempts"


class CpResetpassword(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=1024
    )  # Field name made lowercase.
    randomtoken = models.CharField(
        db_column="RandomToken", max_length=256
    )  # Field name made lowercase.
    requested = models.DateTimeField(
        db_column="Requested"
    )  # Field name made lowercase.
    expires = models.DateTimeField(db_column="Expires")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_ResetPassword"


class CpSessions(models.Model):
    sessionid = models.AutoField(
        db_column="SessionID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    username = models.CharField(
        db_column="Username", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    start = models.DateTimeField(db_column="Start")  # Field name made lowercase.
    expires = models.DateTimeField(db_column="Expires")  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    department = models.CharField(
        db_column="Department", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    claims = models.TextField(
        db_column="Claims", blank=True, null=True
    )  # Field name made lowercase.
    pagesize = models.IntegerField(db_column="PageSize")  # Field name made lowercase.
    isverified = models.BooleanField(
        db_column="IsVerified"
    )  # Field name made lowercase.
    twofaauth = models.ForeignKey(
        "CpTwofaauth",
        models.DO_NOTHING,
        db_column="TwoFAAuth_ID",
        blank=True,
        null=True,
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Sessions"


class CpSettings(models.Model):
    settingkey = models.CharField(
        db_column="SettingKey", primary_key=True, max_length=128
    )  # Field name made lowercase.
    settingvalue = models.TextField(
        db_column="SettingValue", blank=True, null=True
    )  # Field name made lowercase.
    lastupdated = models.DateTimeField(
        db_column="LastUpdated"
    )  # Field name made lowercase.
    lastupdatedby = models.TextField(
        db_column="LastUpdatedBy", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Settings"


class CpSupportnotifications(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    to = models.TextField(db_column="To")  # Field name made lowercase.
    subject = models.TextField(db_column="Subject")  # Field name made lowercase.
    message = models.TextField(db_column="Message")  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    hasbeensent = models.BooleanField(
        db_column="HasBeenSent"
    )  # Field name made lowercase.
    lastsentdate = models.DateTimeField(
        db_column="LastSentDate", blank=True, null=True
    )  # Field name made lowercase.
    notificationtype = models.IntegerField(
        db_column="NotificationType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_SupportNotifications"


class CpTwofaauth(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    uniquekey = models.TextField(
        db_column="UniqueKey", blank=True, null=True
    )  # Field name made lowercase.
    backupcodes = models.TextField(
        db_column="BackupCodes", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_TwoFAAuth"


class CpUsernotificationpins(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userid = models.ForeignKey(
        "CpUsers", models.DO_NOTHING, db_column="UserID"
    )  # Field name made lowercase.
    identifier = models.IntegerField(
        db_column="Identifier"
    )  # Field name made lowercase.
    objecttype = models.IntegerField(
        db_column="ObjectType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_UserNotificationPins"
        unique_together = (("userid", "identifier", "objecttype"),)


class CpUsernotificationtriggers(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    created = models.DateTimeField(db_column="Created")  # Field name made lowercase.
    objectid = models.IntegerField(db_column="ObjectId")  # Field name made lowercase.
    objecttype = models.IntegerField(
        db_column="ObjectType"
    )  # Field name made lowercase.
    objectaction = models.IntegerField(
        db_column="ObjectAction"
    )  # Field name made lowercase.
    message = models.CharField(
        db_column="Message", max_length=256
    )  # Field name made lowercase.
    processed = models.BooleanField(db_column="Processed")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_UserNotificationTriggers"


class CpUserprofile(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    pagesize = models.IntegerField(db_column="PageSize")  # Field name made lowercase.
    backupemail = models.TextField(
        db_column="BackupEmail", blank=True, null=True
    )  # Field name made lowercase.
    lastloginip = models.TextField(
        db_column="LastLoginIp", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_UserProfile"


class CpUsers(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    samaccountname = models.CharField(
        db_column="sAMAccountName", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=64
    )  # Field name made lowercase.
    distinguishedname = models.CharField(
        db_column="DistinguishedName", max_length=255, blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256
    )  # Field name made lowercase.
    firstname = models.CharField(
        db_column="Firstname", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    middlename = models.CharField(
        db_column="Middlename", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    lastname = models.CharField(
        db_column="Lastname", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    email = models.CharField(
        db_column="Email", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    department = models.CharField(
        db_column="Department", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    isreselleradmin = models.BooleanField(
        db_column="IsResellerAdmin", blank=True, null=True
    )  # Field name made lowercase.
    iscompanyadmin = models.BooleanField(
        db_column="IsCompanyAdmin", blank=True, null=True
    )  # Field name made lowercase.
    mailboxplan = models.IntegerField(
        db_column="MailboxPlan", blank=True, null=True
    )  # Field name made lowercase.
    lyncplan = models.IntegerField(
        db_column="LyncPlan", blank=True, null=True
    )  # Field name made lowercase.
    created = models.DateTimeField(
        db_column="Created", blank=True, null=True
    )  # Field name made lowercase.
    additionalmb = models.IntegerField(
        db_column="AdditionalMB", blank=True, null=True
    )  # Field name made lowercase.
    activesyncplan = models.IntegerField(
        db_column="ActiveSyncPlan", blank=True, null=True
    )  # Field name made lowercase.
    isenabled = models.BooleanField(
        db_column="IsEnabled", blank=True, null=True
    )  # Field name made lowercase.
    street = models.CharField(
        db_column="Street", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    city = models.CharField(
        db_column="City", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    state = models.CharField(
        db_column="State", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    postalcode = models.CharField(
        db_column="PostalCode", max_length=40, blank=True, null=True
    )  # Field name made lowercase.
    country = models.CharField(
        db_column="Country", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    company = models.CharField(
        db_column="Company", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    jobtitle = models.CharField(
        db_column="JobTitle", max_length=128, blank=True, null=True
    )  # Field name made lowercase.
    telephonenumber = models.CharField(
        db_column="TelephoneNumber", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    fax = models.CharField(
        db_column="Fax", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    homephone = models.CharField(
        db_column="HomePhone", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    mobilephone = models.CharField(
        db_column="MobilePhone", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    notes = models.TextField(
        db_column="Notes", blank=True, null=True
    )  # Field name made lowercase.
    roleid = models.ForeignKey(
        "UserRoles", models.DO_NOTHING, db_column="RoleID", blank=True, null=True
    )  # Field name made lowercase.
    archiveplan = models.IntegerField(
        db_column="ArchivePlan", blank=True, null=True
    )  # Field name made lowercase.
    skype = models.CharField(
        db_column="Skype", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    facebook = models.CharField(
        db_column="Facebook", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    twitter = models.CharField(
        db_column="Twitter", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    dribbble = models.CharField(
        db_column="Dribbble", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    tumblr = models.CharField(
        db_column="Tumblr", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    linkedin = models.CharField(
        db_column="LinkedIn", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    passwordneverexpires = models.BooleanField(
        db_column="PasswordNeverExpires", blank=True, null=True
    )  # Field name made lowercase.
    exchangeguid = models.CharField(
        db_column="ExchangeGuid", max_length=36
    )  # Field name made lowercase.
    islitigationholdenabled = models.BooleanField(
        db_column="IsLitigationHoldEnabled"
    )  # Field name made lowercase.
    islockedout = models.BooleanField(
        db_column="IsLockedOut", blank=True, null=True
    )  # Field name made lowercase.
    disableddate = models.DateTimeField(
        db_column="DisabledDate", blank=True, null=True
    )  # Field name made lowercase.
    office = models.TextField(
        db_column="Office", blank=True, null=True
    )  # Field name made lowercase.
    retentionid = models.ForeignKey(
        "ExchRetentionpolicies",
        models.DO_NOTHING,
        db_column="RetentionId",
        blank=True,
        null=True,
    )  # Field name made lowercase.
    passwordlastset = models.DateTimeField(
        db_column="PasswordLastSet", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "cp_Users"


class DelayedUsertasks(models.Model):
    taskid = models.AutoField(
        db_column="TaskID", primary_key=True
    )  # Field name made lowercase.
    userid = models.ForeignKey(
        CpUsers, models.DO_NOTHING, db_column="UserID"
    )  # Field name made lowercase.
    status = models.IntegerField(db_column="Status")  # Field name made lowercase.
    lastmessage = models.TextField(
        db_column="LastMessage", blank=True, null=True
    )  # Field name made lowercase.
    created = models.DateTimeField(db_column="Created")  # Field name made lowercase.
    delayeduntil = models.DateTimeField(
        db_column="DelayedUntil"
    )  # Field name made lowercase.
    lastupdated = models.DateTimeField(
        db_column="LastUpdated", blank=True, null=True
    )  # Field name made lowercase.
    tasktype = models.IntegerField(db_column="TaskType")  # Field name made lowercase.
    tasktype_old = models.IntegerField(
        db_column="TaskType_Old"
    )  # Field name made lowercase.
    acknowledged = models.BooleanField(
        db_column="Acknowledged"
    )  # Field name made lowercase.
    acknowledgedby = models.TextField(
        db_column="AcknowledgedBy"
    )  # Field name made lowercase.
    acknowledgedon = models.DateTimeField(
        db_column="AcknowledgedOn", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "delayed_UserTasks"


class DocCategories(models.Model):
    categoryid = models.AutoField(
        db_column="CategoryId", primary_key=True
    )  # Field name made lowercase.
    name = models.TextField(
        db_column="Name", blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_Categories"


class DocFiles(models.Model):
    fileid = models.AutoField(
        db_column="FileId", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    filename = models.TextField(db_column="FileName")  # Field name made lowercase.
    filesize = models.BigIntegerField(
        db_column="FileSize"
    )  # Field name made lowercase.
    filetype = models.TextField(db_column="FileType")  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_Files"


class DocPasswordaudits(models.Model):
    auditid = models.AutoField(
        db_column="AuditId", primary_key=True
    )  # Field name made lowercase.
    passwordid = models.ForeignKey(
        "DocPasswords", models.DO_NOTHING, db_column="PasswordId"
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    user = models.TextField(
        db_column="User", blank=True, null=True
    )  # Field name made lowercase.
    task = models.TextField(
        db_column="Task", blank=True, null=True
    )  # Field name made lowercase.
    note = models.TextField(
        db_column="Note", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_PasswordAudits"


class DocPasswordchangelogs(models.Model):
    changeid = models.AutoField(
        db_column="ChangeId", primary_key=True
    )  # Field name made lowercase.
    passwordid = models.ForeignKey(
        "DocPasswords", models.DO_NOTHING, db_column="PasswordId"
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    user = models.TextField(
        db_column="User", blank=True, null=True
    )  # Field name made lowercase.
    oldpassword = models.TextField(
        db_column="OldPassword", blank=True, null=True
    )  # Field name made lowercase.
    key = models.TextField(
        db_column="Key", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_PasswordChangeLogs"


class DocPasswords(models.Model):
    passwordid = models.AutoField(
        db_column="PasswordId", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    categoryid = models.ForeignKey(
        DocCategories, models.DO_NOTHING, db_column="CategoryId"
    )  # Field name made lowercase.
    username = models.TextField(db_column="Username")  # Field name made lowercase.
    password = models.TextField(db_column="Password")  # Field name made lowercase.
    key = models.TextField(db_column="Key")  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    url = models.TextField(
        db_column="Url", blank=True, null=True
    )  # Field name made lowercase.
    notes = models.TextField(
        db_column="Notes", blank=True, null=True
    )  # Field name made lowercase.
    lastpasswordchange = models.DateTimeField(
        db_column="LastPasswordChange", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_Passwords"


class DocTutorialchangelogs(models.Model):
    changeid = models.AutoField(
        db_column="ChangeId", primary_key=True
    )  # Field name made lowercase.
    tutorialid = models.ForeignKey(
        "DocTutorials", models.DO_NOTHING, db_column="TutorialId"
    )  # Field name made lowercase.
    timestamp = models.DateTimeField(
        db_column="TimeStamp"
    )  # Field name made lowercase.
    user = models.TextField(
        db_column="User", blank=True, null=True
    )  # Field name made lowercase.
    oldheader = models.TextField(
        db_column="OldHeader", blank=True, null=True
    )  # Field name made lowercase.
    oldcontent = models.TextField(
        db_column="OldContent", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_TutorialChangeLogs"


class DocProductkeys(models.Model):
    keyid = models.AutoField(
        db_column="KeyId", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    serialkey = models.TextField(db_column="SerialKey")  # Field name made lowercase.
    serialkey2 = models.TextField(
        db_column="SerialKey2", blank=True, null=True
    )  # Field name made lowercase.
    quantity = models.IntegerField(db_column="Quantity")  # Field name made lowercase.
    purchased = models.DateTimeField(
        db_column="Purchased"
    )  # Field name made lowercase.
    expires = models.DateTimeField(
        db_column="Expires", blank=True, null=True
    )  # Field name made lowercase.
    notes = models.TextField(
        db_column="Notes", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_productkeys"


class DocSslcertificates(models.Model):
    certificateid = models.AutoField(
        db_column="CertificateId", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    vendor = models.TextField(db_column="Vendor")  # Field name made lowercase.
    url = models.CharField(
        db_column="Url", max_length=2083, blank=True, null=True
    )  # Field name made lowercase.
    purchaseprice = models.DecimalField(
        db_column="PurchasePrice", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    purchased = models.DateTimeField(
        db_column="Purchased"
    )  # Field name made lowercase.
    expires = models.DateTimeField(db_column="Expires")  # Field name made lowercase.
    originalcsr = models.TextField(
        db_column="OriginalCSR", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_sslcertificates"


class DocTutorials(models.Model):
    tutorialid = models.AutoField(
        db_column="TutorialId", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId"
    )  # Field name made lowercase.
    header = models.TextField(db_column="Header")  # Field name made lowercase.
    subheader = models.TextField(
        db_column="SubHeader", blank=True, null=True
    )  # Field name made lowercase.
    content = models.TextField(
        db_column="Content", blank=True, null=True
    )  # Field name made lowercase.
    categoryid = models.ForeignKey(
        DocCategories, models.DO_NOTHING, db_column="CategoryId", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "doc_tutorials"


class ExchActivesyncdevices(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userid = models.ForeignKey(
        CpUsers, models.DO_NOTHING, db_column="UserID"
    )  # Field name made lowercase.
    deviceguid = models.CharField(
        db_column="DeviceGuid", max_length=36
    )  # Field name made lowercase.
    firstsynctime = models.DateTimeField(
        db_column="FirstSyncTime", blank=True, null=True
    )  # Field name made lowercase.
    lastpolicyupdatetime = models.DateTimeField(
        db_column="LastPolicyUpdateTime", blank=True, null=True
    )  # Field name made lowercase.
    lastsyncattempttime = models.DateTimeField(
        db_column="LastSyncAttemptTime", blank=True, null=True
    )  # Field name made lowercase.
    lastsuccesssync = models.DateTimeField(
        db_column="LastSuccessSync", blank=True, null=True
    )  # Field name made lowercase.
    devicewipesenttime = models.DateTimeField(
        db_column="DeviceWipeSentTime", blank=True, null=True
    )  # Field name made lowercase.
    devicewiperequesttime = models.DateTimeField(
        db_column="DeviceWipeRequestTime", blank=True, null=True
    )  # Field name made lowercase.
    devicewipeacktime = models.DateTimeField(
        db_column="DeviceWipeAckTime", blank=True, null=True
    )  # Field name made lowercase.
    lastpingheartbeat = models.IntegerField(
        db_column="LastPingHeartbeat"
    )  # Field name made lowercase.
    identity = models.TextField(
        db_column="Identity", blank=True, null=True
    )  # Field name made lowercase.
    devicetype = models.TextField(
        db_column="DeviceType", blank=True, null=True
    )  # Field name made lowercase.
    deviceid = models.TextField(
        db_column="DeviceID", blank=True, null=True
    )  # Field name made lowercase.
    deviceuseragent = models.TextField(
        db_column="DeviceUserAgent", blank=True, null=True
    )  # Field name made lowercase.
    devicemodel = models.TextField(
        db_column="DeviceModel", blank=True, null=True
    )  # Field name made lowercase.
    deviceimei = models.TextField(
        db_column="DeviceImei", blank=True, null=True
    )  # Field name made lowercase.
    devicefriendlyname = models.TextField(
        db_column="DeviceFriendlyName", blank=True, null=True
    )  # Field name made lowercase.
    deviceos = models.TextField(
        db_column="DeviceOS", blank=True, null=True
    )  # Field name made lowercase.
    devicephonenumber = models.TextField(
        db_column="DevicePhoneNumber", blank=True, null=True
    )  # Field name made lowercase.
    status = models.TextField(
        db_column="Status", blank=True, null=True
    )  # Field name made lowercase.
    statusnote = models.TextField(
        db_column="StatusNote", blank=True, null=True
    )  # Field name made lowercase.
    devicepolicyapplied = models.TextField(
        db_column="DevicePolicyApplied", blank=True, null=True
    )  # Field name made lowercase.
    devicepolicyapplicationstatus = models.TextField(
        db_column="DevicePolicyApplicationStatus", blank=True, null=True
    )  # Field name made lowercase.
    deviceactivesyncversion = models.TextField(
        db_column="DeviceActiveSyncVersion", blank=True, null=True
    )  # Field name made lowercase.
    numberoffolderssynced = models.TextField(
        db_column="NumberOfFoldersSynced", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_ActiveSyncDevices"


class ExchContacts(models.Model):
    distinguishedname = models.TextField(
        db_column="DistinguishedName"
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256
    )  # Field name made lowercase.
    email = models.CharField(
        db_column="Email", max_length=256
    )  # Field name made lowercase.
    hidden = models.BooleanField(db_column="Hidden")  # Field name made lowercase.
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_Contacts"


class ExchDatabases(models.Model):
    databaseguid = models.CharField(
        db_column="DatabaseGuid", primary_key=True, max_length=36
    )  # Field name made lowercase.
    siteid = models.IntegerField(
        db_column="SiteId", blank=True, null=True
    )  # Field name made lowercase.
    identity = models.TextField(db_column="Identity")  # Field name made lowercase.
    edbfilepath = models.TextField(
        db_column="EdbFilePath"
    )  # Field name made lowercase.
    logfolderpath = models.TextField(
        db_column="LogFolderPath", blank=True, null=True
    )  # Field name made lowercase.
    mountedonserver = models.TextField(
        db_column="MountedOnServer"
    )  # Field name made lowercase.
    mounted = models.BooleanField(db_column="Mounted")  # Field name made lowercase.
    ismailboxdatabase = models.BooleanField(
        db_column="IsMailboxDatabase"
    )  # Field name made lowercase.
    ispublicfolderdatabase = models.BooleanField(
        db_column="IsPublicFolderDatabase"
    )  # Field name made lowercase.
    isexcludedfromprovisioning = models.BooleanField(
        db_column="IsExcludedFromProvisioning"
    )  # Field name made lowercase.
    issuspendedfromprovisioning = models.BooleanField(
        db_column="IsSuspendedFromProvisioning"
    )  # Field name made lowercase.
    circularloggingenabled = models.BooleanField(
        db_column="CircularLoggingEnabled"
    )  # Field name made lowercase.
    databasesizeinbytes = models.BigIntegerField(
        db_column="DatabaseSizeInBytes"
    )  # Field name made lowercase.
    availablenewmailboxspaceinbytes = models.BigIntegerField(
        db_column="AvailableNewMailboxSpaceInBytes"
    )  # Field name made lowercase.
    lastfullbackup = models.DateTimeField(
        db_column="LastFullBackup", blank=True, null=True
    )  # Field name made lowercase.
    lastcopybackup = models.DateTimeField(
        db_column="LastCopyBackup", blank=True, null=True
    )  # Field name made lowercase.
    lastretrieved = models.DateTimeField(
        db_column="LastRetrieved", blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.TextField(
        db_column="CompanyCode", blank=True, null=True
    )  # Field name made lowercase.
    lastused = models.DateTimeField(
        db_column="LastUsed", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_Databases"


class ExchDistributiongroups(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    distinguishedname = models.TextField(
        db_column="DistinguishedName"
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256
    )  # Field name made lowercase.
    email = models.CharField(
        db_column="Email", max_length=256
    )  # Field name made lowercase.
    hidden = models.BooleanField(db_column="Hidden")  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36
    )  # Field name made lowercase.
    issecuritygroup = models.BooleanField(
        db_column="IsSecurityGroup"
    )  # Field name made lowercase.
    grouptype = models.IntegerField(db_column="GroupType")  # Field name made lowercase.
    iscpprotected = models.BooleanField(
        db_column="IsCpProtected"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_DistributionGroups"


class ExchPublicfoldermailboxes(models.Model):
    mailboxid = models.AutoField(
        db_column="MailboxID", primary_key=True
    )  # Field name made lowercase.
    companyid = models.ForeignKey(
        CpCompanies, models.DO_NOTHING, db_column="CompanyID", blank=True, null=True
    )  # Field name made lowercase.
    planid = models.ForeignKey(
        "PlansExchpublicfolders", models.DO_NOTHING, db_column="PlanID"
    )  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36
    )  # Field name made lowercase.
    exchangeguid = models.CharField(
        db_column="ExchangeGuid", max_length=36
    )  # Field name made lowercase.
    distinguishedname = models.TextField(
        db_column="DistinguishedName", blank=True, null=True
    )  # Field name made lowercase.
    name = models.TextField(
        db_column="Name", blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.TextField(
        db_column="DisplayName", blank=True, null=True
    )  # Field name made lowercase.
    userprincipalname = models.TextField(
        db_column="UserPrincipalName", blank=True, null=True
    )  # Field name made lowercase.
    emailaddress = models.TextField(
        db_column="EmailAddress", blank=True, null=True
    )  # Field name made lowercase.
    rootfolder = models.TextField(
        db_column="RootFolder", blank=True, null=True
    )  # Field name made lowercase.
    isrootpublicfoldermailbox = models.BooleanField(
        db_column="IsRootPublicFolderMailbox"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_PublicFolderMailboxes"


class ExchPublicfolders(models.Model):
    publicfolderid = models.AutoField(
        db_column="PublicFolderID", primary_key=True
    )  # Field name made lowercase.
    mailboxid = models.ForeignKey(
        ExchPublicfoldermailboxes, models.DO_NOTHING, db_column="MailboxID"
    )  # Field name made lowercase.
    objectguid = models.CharField(
        db_column="ObjectGuid", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    identity = models.TextField(
        db_column="Identity", blank=True, null=True
    )  # Field name made lowercase.
    name = models.TextField(
        db_column="Name", blank=True, null=True
    )  # Field name made lowercase.
    parentpath = models.TextField(
        db_column="ParentPath", blank=True, null=True
    )  # Field name made lowercase.
    emailaddress = models.TextField(
        db_column="EmailAddress", blank=True, null=True
    )  # Field name made lowercase.
    mailenabled = models.BooleanField(
        db_column="MailEnabled"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_PublicFolders"


class ExchResourcemailboxes(models.Model):
    resourceid = models.AutoField(
        db_column="ResourceID", primary_key=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=255
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=255
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=255
    )  # Field name made lowercase.
    primarysmtpaddress = models.CharField(
        db_column="PrimarySmtpAddress", max_length=255
    )  # Field name made lowercase.
    resourcetype = models.CharField(
        db_column="ResourceType", max_length=10
    )  # Field name made lowercase.
    mailboxplan = models.IntegerField(
        db_column="MailboxPlan"
    )  # Field name made lowercase.
    additionalmb = models.IntegerField(
        db_column="AdditionalMB"
    )  # Field name made lowercase.
    distinguishedname = models.TextField(
        db_column="DistinguishedName", blank=True, null=True
    )  # Field name made lowercase.
    resourceguid = models.CharField(
        db_column="ResourceGuid", max_length=36, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_ResourceMailboxes"


class ExchRetentionpolicies(models.Model):
    retentionid = models.CharField(
        db_column="RetentionId", primary_key=True, max_length=36
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_RetentionPolicies"


class ExchRetentiontagtopolicy(models.Model):
    tagguid = models.OneToOneField(
        "ExchRetentiontags", models.DO_NOTHING, db_column="TagGuid", primary_key=True
    )  # Field name made lowercase.
    retentionid = models.ForeignKey(
        ExchRetentionpolicies, models.DO_NOTHING, db_column="RetentionId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_RetentionTagToPolicy"
        unique_together = (("tagguid", "retentionid"),)


class ExchRetentiontags(models.Model):
    tagguid = models.CharField(
        db_column="TagGuid", primary_key=True, max_length=36
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    comment = models.TextField(
        db_column="Comment", blank=True, null=True
    )  # Field name made lowercase.
    retentionaction = models.TextField(
        db_column="RetentionAction"
    )  # Field name made lowercase.
    agelimitforretention = models.BigIntegerField(
        db_column="AgeLimitForRetention", blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    retentiontype = models.TextField(
        db_column="RetentionType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "exch_RetentionTags"


class PlansCompany(models.Model):
    orgplanid = models.AutoField(
        db_column="OrgPlanID", primary_key=True
    )  # Field name made lowercase.
    orgplanname = models.CharField(
        db_column="OrgPlanName", max_length=50
    )  # Field name made lowercase.
    productid = models.IntegerField(db_column="ProductID")  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    maxusers = models.IntegerField(db_column="MaxUsers")  # Field name made lowercase.
    maxdomains = models.IntegerField(
        db_column="MaxDomains"
    )  # Field name made lowercase.
    maxexchangemailboxes = models.IntegerField(
        db_column="MaxExchangeMailboxes"
    )  # Field name made lowercase.
    maxexchangecontacts = models.IntegerField(
        db_column="MaxExchangeContacts"
    )  # Field name made lowercase.
    maxexchangedistlists = models.IntegerField(
        db_column="MaxExchangeDistLists"
    )  # Field name made lowercase.
    maxexchangepublicfolders = models.IntegerField(
        db_column="MaxExchangePublicFolders"
    )  # Field name made lowercase.
    maxexchangemailpublicfolders = models.IntegerField(
        db_column="MaxExchangeMailPublicFolders"
    )  # Field name made lowercase.
    maxexchangekeepdeleteditems = models.IntegerField(
        db_column="MaxExchangeKeepDeletedItems"
    )  # Field name made lowercase.
    maxexchangeactivesyncpolicies = models.IntegerField(
        db_column="MaxExchangeActivesyncPolicies", blank=True, null=True
    )  # Field name made lowercase.
    maxterminalserverusers = models.IntegerField(
        db_column="MaxTerminalServerUsers"
    )  # Field name made lowercase.
    maxexchangeresourcemailboxes = models.IntegerField(
        db_column="MaxExchangeResourceMailboxes", blank=True, null=True
    )  # Field name made lowercase.
    maxsecuritygroups = models.IntegerField(
        db_column="MaxSecurityGroups"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_Company"


class PlansExchactivesync(models.Model):
    asid = models.AutoField(
        db_column="ASID", primary_key=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    exchangename = models.CharField(
        db_column="ExchangeName", max_length=75, blank=True, null=True
    )  # Field name made lowercase.
    allownonprovisionabledevices = models.BooleanField(
        db_column="AllowNonProvisionableDevices"
    )  # Field name made lowercase.
    refreshintervalinhours = models.IntegerField(
        db_column="RefreshIntervalInHours", blank=True, null=True
    )  # Field name made lowercase.
    requirepassword = models.BooleanField(
        db_column="RequirePassword"
    )  # Field name made lowercase.
    requirealphanumericpassword = models.BooleanField(
        db_column="RequireAlphanumericPassword"
    )  # Field name made lowercase.
    enablepasswordrecovery = models.BooleanField(
        db_column="EnablePasswordRecovery"
    )  # Field name made lowercase.
    requireencryptionondevice = models.BooleanField(
        db_column="RequireEncryptionOnDevice"
    )  # Field name made lowercase.
    requireencryptiononstoragecard = models.BooleanField(
        db_column="RequireEncryptionOnStorageCard"
    )  # Field name made lowercase.
    allowsimplepassword = models.BooleanField(
        db_column="AllowSimplePassword"
    )  # Field name made lowercase.
    numberoffailedattempted = models.IntegerField(
        db_column="NumberOfFailedAttempted", blank=True, null=True
    )  # Field name made lowercase.
    minimumpasswordlength = models.IntegerField(
        db_column="MinimumPasswordLength", blank=True, null=True
    )  # Field name made lowercase.
    inactivitytimeoutinminutes = models.IntegerField(
        db_column="InactivityTimeoutInMinutes", blank=True, null=True
    )  # Field name made lowercase.
    passwordexpirationindays = models.IntegerField(
        db_column="PasswordExpirationInDays", blank=True, null=True
    )  # Field name made lowercase.
    enforcepasswordhistory = models.IntegerField(
        db_column="EnforcePasswordHistory", blank=True, null=True
    )  # Field name made lowercase.
    includepastcalendaritems = models.CharField(
        db_column="IncludePastCalendarItems", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    includepastemailitems = models.CharField(
        db_column="IncludePastEmailItems", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    limitemailsizeinkb = models.IntegerField(
        db_column="LimitEmailSizeInKB", blank=True, null=True
    )  # Field name made lowercase.
    allowdirectpushwhenroaming = models.BooleanField(
        db_column="AllowDirectPushWhenRoaming"
    )  # Field name made lowercase.
    allowhtmlemail = models.BooleanField(
        db_column="AllowHTMLEmail"
    )  # Field name made lowercase.
    allowattachmentsdownload = models.BooleanField(
        db_column="AllowAttachmentsDownload"
    )  # Field name made lowercase.
    maximumattachmentsizeinkb = models.IntegerField(
        db_column="MaximumAttachmentSizeInKB", blank=True, null=True
    )  # Field name made lowercase.
    allowremovablestorage = models.BooleanField(
        db_column="AllowRemovableStorage"
    )  # Field name made lowercase.
    allowcamera = models.BooleanField(
        db_column="AllowCamera"
    )  # Field name made lowercase.
    allowwifi = models.BooleanField(db_column="AllowWiFi")  # Field name made lowercase.
    allowinfrared = models.BooleanField(
        db_column="AllowInfrared"
    )  # Field name made lowercase.
    allowinternetsharing = models.BooleanField(
        db_column="AllowInternetSharing"
    )  # Field name made lowercase.
    allowremotedesktop = models.BooleanField(
        db_column="AllowRemoteDesktop"
    )  # Field name made lowercase.
    allowdesktopsync = models.BooleanField(
        db_column="AllowDesktopSync"
    )  # Field name made lowercase.
    allowbluetooth = models.CharField(
        db_column="AllowBluetooth", max_length=10, blank=True, null=True
    )  # Field name made lowercase.
    allowbrowser = models.BooleanField(
        db_column="AllowBrowser"
    )  # Field name made lowercase.
    allowconsumermail = models.BooleanField(
        db_column="AllowConsumerMail"
    )  # Field name made lowercase.
    isenterprisecal = models.BooleanField(
        db_column="IsEnterpriseCAL", blank=True, null=True
    )  # Field name made lowercase.
    allowtextmessaging = models.BooleanField(
        db_column="AllowTextMessaging"
    )  # Field name made lowercase.
    allowunsignedapplications = models.BooleanField(
        db_column="AllowUnsignedApplications"
    )  # Field name made lowercase.
    allowunsignedinstallationpackages = models.BooleanField(
        db_column="AllowUnsignedInstallationPackages"
    )  # Field name made lowercase.
    mindevicepasswordcomplexcharacters = models.IntegerField(
        db_column="MinDevicePasswordComplexCharacters", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_ExchActiveSync"


class PlansExcharchiving(models.Model):
    archivingid = models.AutoField(
        db_column="ArchivingID", primary_key=True
    )  # Field name made lowercase.
    displayname = models.CharField(
        db_column="DisplayName", max_length=256
    )  # Field name made lowercase.
    database = models.TextField(
        db_column="Database", blank=True, null=True
    )  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description"
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    cost = models.DecimalField(
        db_column="Cost", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    archivesizemb = models.IntegerField(
        db_column="ArchiveSizeMB"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_ExchArchiving"


class PlansExchmailbox(models.Model):
    mailboxplanid = models.AutoField(
        db_column="MailboxPlanID", primary_key=True
    )  # Field name made lowercase.
    mailboxplanname = models.CharField(
        db_column="MailboxPlanName", max_length=50
    )  # Field name made lowercase.
    productid = models.IntegerField(
        db_column="ProductID", blank=True, null=True
    )  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    prohibitsendreceivequotamb = models.IntegerField(
        db_column="ProhibitSendReceiveQuotaMB"
    )  # Field name made lowercase.
    maxmailboxsizemb = models.IntegerField(
        db_column="MaxMailboxSizeMB", blank=True, null=True
    )  # Field name made lowercase.
    maxsendkb = models.IntegerField(db_column="MaxSendKB")  # Field name made lowercase.
    maxreceivekb = models.IntegerField(
        db_column="MaxReceiveKB"
    )  # Field name made lowercase.
    maxrecipients = models.IntegerField(
        db_column="MaxRecipients"
    )  # Field name made lowercase.
    enablepop3 = models.BooleanField(
        db_column="EnablePOP3"
    )  # Field name made lowercase.
    enableimap = models.BooleanField(
        db_column="EnableIMAP"
    )  # Field name made lowercase.
    enableowa = models.BooleanField(db_column="EnableOWA")  # Field name made lowercase.
    enablemapi = models.BooleanField(
        db_column="EnableMAPI"
    )  # Field name made lowercase.
    enableas = models.BooleanField(db_column="EnableAS")  # Field name made lowercase.
    enableecp = models.BooleanField(db_column="EnableECP")  # Field name made lowercase.
    maxkeepdeleteditems = models.IntegerField(
        db_column="MaxKeepDeletedItems"
    )  # Field name made lowercase.
    mailboxplandesc = models.TextField(
        db_column="MailboxPlanDesc", blank=True, null=True
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    cost = models.DecimalField(
        db_column="Cost", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    additionalgbprice = models.DecimalField(
        db_column="AdditionalGBPrice", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    issuewarningquotamb = models.IntegerField(
        db_column="IssueWarningQuotaMB"
    )  # Field name made lowercase.
    prohibitsendquotamb = models.IntegerField(
        db_column="ProhibitSendQuotaMB"
    )  # Field name made lowercase.
    enableowafordevices = models.BooleanField(
        db_column="EnableOWAForDevices"
    )  # Field name made lowercase.
    enableews = models.BooleanField(db_column="EnableEWS")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_ExchMailbox"


class PlansExchpublicfolders(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    name = models.TextField(db_column="Name")  # Field name made lowercase.
    mailboxsizemb = models.IntegerField(
        db_column="MailboxSizeMB"
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    cost = models.DecimalField(
        db_column="Cost", max_digits=18, decimal_places=2
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_ExchPublicFolders"


class PlansXendesktop(models.Model):
    citrixplanid = models.AutoField(
        db_column="CitrixPlanID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=56
    )  # Field name made lowercase.
    groupname = models.CharField(
        db_column="GroupName", max_length=64
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    isserver = models.BooleanField(db_column="IsServer")  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    cost = models.DecimalField(
        db_column="Cost", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    pictureurl = models.CharField(
        db_column="PictureURL", max_length=255, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "plans_XenDesktop"


class PriceOverride(models.Model):
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=18, decimal_places=2
    )  # Field name made lowercase.
    planid = models.IntegerField(
        db_column="PlanID", blank=True, null=True
    )  # Field name made lowercase.
    product = models.CharField(
        db_column="Product", max_length=25, blank=True, null=True
    )  # Field name made lowercase.
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "price_Override"


class PricePrices(models.Model):
    priceid = models.AutoField(
        db_column="PriceID", primary_key=True
    )  # Field name made lowercase.
    productid = models.IntegerField(db_column="ProductID")  # Field name made lowercase.
    planid = models.IntegerField(db_column="PlanID")  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64
    )  # Field name made lowercase.
    price = models.DecimalField(
        db_column="Price", max_digits=19, decimal_places=4
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "price_Prices"


class SpamMailprotectoraccounts(models.Model):
    companyid = models.OneToOneField(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId", primary_key=True
    )  # Field name made lowercase.
    mpid = models.IntegerField(db_column="MpID")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "spam_MailProtectorAccounts"


class SpamMailprotectorlogcodes(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    code = models.CharField(
        db_column="Code", max_length=256
    )  # Field name made lowercase.
    mode = models.CharField(
        db_column="Mode", max_length=10
    )  # Field name made lowercase.
    weight = models.IntegerField(db_column="Weight")  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=256
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description"
    )  # Field name made lowercase.
    category = models.CharField(
        db_column="Category", max_length=10
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "spam_MailProtectorLogCodes"


class SpamProducts(models.Model):
    companyid = models.OneToOneField(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId", primary_key=True
    )  # Field name made lowercase.
    spamproductid = models.IntegerField(
        db_column="SpamProductId"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "spam_Products"


class StatExchmailboxarchivesizes(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=64
    )  # Field name made lowercase.
    mailboxdatabase = models.CharField(
        db_column="MailboxDatabase", max_length=255
    )  # Field name made lowercase.
    totalitemsize = models.CharField(
        db_column="TotalItemSize", max_length=255
    )  # Field name made lowercase.
    totalitemsizeinbytes = models.BigIntegerField(
        db_column="TotalItemSizeInBytes"
    )  # Field name made lowercase.
    totaldeleteditemsize = models.CharField(
        db_column="TotalDeletedItemSize", max_length=255
    )  # Field name made lowercase.
    totaldeleteditemsizeinbytes = models.BigIntegerField(
        db_column="TotalDeletedItemSizeInBytes"
    )  # Field name made lowercase.
    itemcount = models.IntegerField(db_column="ItemCount")  # Field name made lowercase.
    deleteditemcount = models.IntegerField(
        db_column="DeletedItemCount"
    )  # Field name made lowercase.
    retrieved = models.DateTimeField(
        db_column="Retrieved"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "stat_ExchMailboxArchiveSizes"


class StatExchmailboxdatabasesizes(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    databasename = models.CharField(
        db_column="DatabaseName", max_length=64
    )  # Field name made lowercase.
    server = models.CharField(
        db_column="Server", max_length=64
    )  # Field name made lowercase.
    databasesize = models.CharField(
        db_column="DatabaseSize", max_length=255
    )  # Field name made lowercase.
    retrieved = models.DateTimeField(
        db_column="Retrieved"
    )  # Field name made lowercase.
    databasesizeinbytes = models.BigIntegerField(
        db_column="DatabaseSizeInBytes"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "stat_ExchMailboxDatabaseSizes"


class StatExchmailboxsizes(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userguid = models.CharField(
        db_column="UserGuid", max_length=36
    )  # Field name made lowercase.
    userprincipalname = models.CharField(
        db_column="UserPrincipalName", max_length=64
    )  # Field name made lowercase.
    mailboxdatabase = models.CharField(
        db_column="MailboxDatabase", max_length=255
    )  # Field name made lowercase.
    totalitemsize = models.CharField(
        db_column="TotalItemSize", max_length=255
    )  # Field name made lowercase.
    totalitemsizeinbytes = models.BigIntegerField(
        db_column="TotalItemSizeInBytes"
    )  # Field name made lowercase.
    totaldeleteditemsize = models.CharField(
        db_column="TotalDeletedItemSize", max_length=255
    )  # Field name made lowercase.
    totaldeleteditemsizeinbytes = models.BigIntegerField(
        db_column="TotalDeletedItemSizeInBytes"
    )  # Field name made lowercase.
    itemcount = models.IntegerField(db_column="ItemCount")  # Field name made lowercase.
    deleteditemcount = models.IntegerField(
        db_column="DeletedItemCount"
    )  # Field name made lowercase.
    retrieved = models.DateTimeField(
        db_column="Retrieved"
    )  # Field name made lowercase.
    lastlogontime = models.DateTimeField(
        db_column="LastLogonTime", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "stat_ExchMailboxSizes"


class StatExchmessagetrackingcounts(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    userid = models.ForeignKey(
        CpUsers, models.DO_NOTHING, db_column="UserID"
    )  # Field name made lowercase.
    totalsent = models.IntegerField(db_column="TotalSent")  # Field name made lowercase.
    totalreceived = models.IntegerField(
        db_column="TotalReceived"
    )  # Field name made lowercase.
    start = models.DateTimeField(db_column="Start")  # Field name made lowercase.
    end = models.DateTimeField(db_column="End")  # Field name made lowercase.
    totalbytessent = models.BigIntegerField(
        db_column="TotalBytesSent"
    )  # Field name made lowercase.
    totalbytesreceived = models.BigIntegerField(
        db_column="TotalBytesReceived"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "stat_ExchMessageTrackingCounts"


class StatHistorystatistics(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    retrieved = models.DateTimeField(
        db_column="Retrieved"
    )  # Field name made lowercase.
    usercount = models.IntegerField(db_column="UserCount")  # Field name made lowercase.
    mailboxcount = models.IntegerField(
        db_column="MailboxCount"
    )  # Field name made lowercase.
    citrixcount = models.IntegerField(
        db_column="CitrixCount"
    )  # Field name made lowercase.
    resellercode = models.CharField(
        db_column="ResellerCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    companycode = models.CharField(
        db_column="CompanyCode", max_length=64, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "stat_HistoryStatistics"


class TpCwagreements(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    cwcompanyid = models.ForeignKey(
        "TpCwcompanies", models.DO_NOTHING, db_column="CWCompanyId"
    )  # Field name made lowercase.
    cwagreementid = models.IntegerField(
        db_column="CWAgreementId"
    )  # Field name made lowercase.
    cwagreementname = models.TextField(
        db_column="CWAgreementName"
    )  # Field name made lowercase.
    typeofagreement = models.IntegerField(
        db_column="TypeOfAgreement"
    )  # Field name made lowercase.
    lastattempted = models.DateTimeField(
        db_column="LastAttempted", blank=True, null=True
    )  # Field name made lowercase.
    lastresult = models.TextField(
        db_column="LastResult", blank=True, null=True
    )  # Field name made lowercase.
    excludedisabledusers = models.BooleanField(
        db_column="ExcludeDisabledUsers"
    )  # Field name made lowercase.
    includeuserinformationoninvoice = models.BooleanField(
        db_column="IncludeUserInformationOnInvoice"
    )  # Field name made lowercase.
    producttosecuritygroupid = models.ForeignKey(
        "TpCwproducttosecuritygroup",
        models.DO_NOTHING,
        db_column="ProductToSecurityGroupId",
        blank=True,
        null=True,
    )  # Field name made lowercase.
    includemailboxusersonly = models.BooleanField(
        db_column="IncludeMailboxUsersOnly"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWAgreements"


class TpCwcompanies(models.Model):
    companyid = models.OneToOneField(
        CpCompanies, models.DO_NOTHING, db_column="CompanyId", primary_key=True
    )  # Field name made lowercase.
    cwcompanyid = models.IntegerField(
        db_column="CWCompanyId"
    )  # Field name made lowercase.
    cwcompanyname = models.TextField(
        db_column="CWCompanyName", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWCompanies"


class TpCwproducttocitrixgroup(models.Model):
    desktopgroupid = models.OneToOneField(
        CitrixDesktopgroups,
        models.DO_NOTHING,
        db_column="DesktopGroupID",
        primary_key=True,
    )  # Field name made lowercase.
    cwproductid = models.IntegerField(
        db_column="CWProductId"
    )  # Field name made lowercase.
    cwdescription = models.CharField(
        db_column="CWDescription", max_length=2000
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWProductToCitrixGroup"


class TpCwproducttomailboxplan(models.Model):
    mailboxplanid = models.OneToOneField(
        PlansExchmailbox, models.DO_NOTHING, db_column="MailboxPlanID", primary_key=True
    )  # Field name made lowercase.
    cwproductid = models.IntegerField(
        db_column="CWProductId"
    )  # Field name made lowercase.
    cwdescription = models.CharField(
        db_column="CWDescription", max_length=2000
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWProductToMailboxPlan"


class TpCwproducttosecuritygroup(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    securitygroupid = models.ForeignKey(
        AdSecuritygroups, models.DO_NOTHING, db_column="SecurityGroupID"
    )  # Field name made lowercase.
    cwproductid = models.IntegerField(
        db_column="CWProductId"
    )  # Field name made lowercase.
    cwproductdescription = models.TextField(
        db_column="CWProductDescription"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWProductToSecurityGroup"


class TpCwsettings(models.Model):
    settingkey = models.CharField(
        db_column="SettingKey", primary_key=True, max_length=128
    )  # Field name made lowercase.
    settingvalue = models.TextField(
        db_column="SettingValue", blank=True, null=True
    )  # Field name made lowercase.
    description = models.TextField(
        db_column="Description", blank=True, null=True
    )  # Field name made lowercase.
    lastupdated = models.DateTimeField(
        db_column="LastUpdated"
    )  # Field name made lowercase.
    lastupdatedby = models.TextField(
        db_column="LastUpdatedBy", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tp_CWSettings"


class UserPermissions(models.Model):
    userid = models.IntegerField(
        db_column="UserID", primary_key=True
    )  # Field name made lowercase.
    roleid = models.IntegerField(db_column="RoleID")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "user_Permissions"


class UserRoles(models.Model):
    roleid = models.AutoField(
        db_column="RoleID", primary_key=True
    )  # Field name made lowercase.
    displayname = models.TextField(
        db_column="DisplayName", blank=True, null=True
    )  # Field name made lowercase.
    editpermissions = models.BooleanField(
        db_column="EditPermissions"
    )  # Field name made lowercase.
    isreseller = models.BooleanField(
        db_column="IsReseller"
    )  # Field name made lowercase.
    addcompanies = models.BooleanField(
        db_column="AddCompanies"
    )  # Field name made lowercase.
    updatecompanies = models.BooleanField(
        db_column="UpdateCompanies"
    )  # Field name made lowercase.
    deletecompanies = models.BooleanField(
        db_column="DeleteCompanies"
    )  # Field name made lowercase.
    viewcompanybilling = models.BooleanField(
        db_column="ViewCompanyBilling"
    )  # Field name made lowercase.
    updatecompanybilling = models.BooleanField(
        db_column="UpdateCompanyBilling"
    )  # Field name made lowercase.
    viewuser = models.BooleanField(db_column="ViewUser")  # Field name made lowercase.
    createuser = models.BooleanField(
        db_column="CreateUser"
    )  # Field name made lowercase.
    deleteuser = models.BooleanField(
        db_column="DeleteUser"
    )  # Field name made lowercase.
    updateuser = models.BooleanField(
        db_column="UpdateUser"
    )  # Field name made lowercase.
    resetpassword = models.BooleanField(
        db_column="ResetPassword"
    )  # Field name made lowercase.
    resetlogin = models.BooleanField(
        db_column="ResetLogin"
    )  # Field name made lowercase.
    viewdomain = models.BooleanField(
        db_column="ViewDomain"
    )  # Field name made lowercase.
    updatedomain = models.BooleanField(
        db_column="UpdateDomain"
    )  # Field name made lowercase.
    createdomain = models.BooleanField(
        db_column="CreateDomain"
    )  # Field name made lowercase.
    deletedomain = models.BooleanField(
        db_column="DeleteDomain"
    )  # Field name made lowercase.
    viewcontact = models.BooleanField(
        db_column="ViewContact"
    )  # Field name made lowercase.
    viewgroup = models.BooleanField(db_column="ViewGroup")  # Field name made lowercase.
    viewresource = models.BooleanField(
        db_column="ViewResource"
    )  # Field name made lowercase.
    viewpublicfolder = models.BooleanField(
        db_column="ViewPublicFolder"
    )  # Field name made lowercase.
    viewactivesync = models.BooleanField(
        db_column="ViewActiveSync"
    )  # Field name made lowercase.
    createcontact = models.BooleanField(
        db_column="CreateContact"
    )  # Field name made lowercase.
    creategroup = models.BooleanField(
        db_column="CreateGroup"
    )  # Field name made lowercase.
    createresource = models.BooleanField(
        db_column="CreateResource"
    )  # Field name made lowercase.
    createpublicfolder = models.BooleanField(
        db_column="CreatePublicFolder"
    )  # Field name made lowercase.
    createactivesync = models.BooleanField(
        db_column="CreateActiveSync"
    )  # Field name made lowercase.
    updatecontact = models.BooleanField(
        db_column="UpdateContact"
    )  # Field name made lowercase.
    updategroup = models.BooleanField(
        db_column="UpdateGroup"
    )  # Field name made lowercase.
    updateresource = models.BooleanField(
        db_column="UpdateResource"
    )  # Field name made lowercase.
    updatepublicfolder = models.BooleanField(
        db_column="UpdatePublicFolder"
    )  # Field name made lowercase.
    updateactivesync = models.BooleanField(
        db_column="UpdateActiveSync"
    )  # Field name made lowercase.
    deletecontact = models.BooleanField(
        db_column="DeleteContact"
    )  # Field name made lowercase.
    deletegroup = models.BooleanField(
        db_column="DeleteGroup"
    )  # Field name made lowercase.
    deleteresource = models.BooleanField(
        db_column="DeleteResource"
    )  # Field name made lowercase.
    deletepublicfolder = models.BooleanField(
        db_column="DeletePublicFolder"
    )  # Field name made lowercase.
    deleteactivesync = models.BooleanField(
        db_column="DeleteActiveSync"
    )  # Field name made lowercase.
    enableexchange = models.BooleanField(
        db_column="EnableExchange"
    )  # Field name made lowercase.
    disableexchange = models.BooleanField(
        db_column="DisableExchange"
    )  # Field name made lowercase.
    viewcitrix = models.BooleanField(
        db_column="ViewCitrix"
    )  # Field name made lowercase.
    logoffcitrix = models.BooleanField(
        db_column="LogOffCitrix"
    )  # Field name made lowercase.
    assigncitrix = models.BooleanField(
        db_column="AssignCitrix"
    )  # Field name made lowercase.
    sendmessagecitrix = models.BooleanField(
        db_column="SendMessageCitrix"
    )  # Field name made lowercase.
    removecitrix = models.BooleanField(
        db_column="RemoveCitrix"
    )  # Field name made lowercase.
    viewsecuritygroup = models.BooleanField(
        db_column="ViewSecurityGroup"
    )  # Field name made lowercase.
    updatesecuritygroup = models.BooleanField(
        db_column="UpdateSecurityGroup"
    )  # Field name made lowercase.
    createsecuritygroup = models.BooleanField(
        db_column="CreateSecurityGroup"
    )  # Field name made lowercase.
    deletesecuritygroup = models.BooleanField(
        db_column="DeleteSecurityGroup"
    )  # Field name made lowercase.
    addsecuritygroupmember = models.BooleanField(
        db_column="AddSecurityGroupMember"
    )  # Field name made lowercase.
    deletesecuritygroupmember = models.BooleanField(
        db_column="DeleteSecurityGroupMember"
    )  # Field name made lowercase.
    viewexchangereports = models.BooleanField(
        db_column="ViewExchangeReports"
    )  # Field name made lowercase.
    viewcitrixreports = models.BooleanField(
        db_column="ViewCitrixReports"
    )  # Field name made lowercase.
    viewcertificate = models.BooleanField(
        db_column="ViewCertificate"
    )  # Field name made lowercase.
    updatecertificate = models.BooleanField(
        db_column="UpdateCertificate"
    )  # Field name made lowercase.
    createcertificate = models.BooleanField(
        db_column="CreateCertificate"
    )  # Field name made lowercase.
    deletecertificate = models.BooleanField(
        db_column="DeleteCertificate"
    )  # Field name made lowercase.
    viewfile = models.BooleanField(db_column="ViewFile")  # Field name made lowercase.
    createfile = models.BooleanField(
        db_column="CreateFile"
    )  # Field name made lowercase.
    downloadfile = models.BooleanField(
        db_column="DownloadFile"
    )  # Field name made lowercase.
    updatefile = models.BooleanField(
        db_column="UpdateFile"
    )  # Field name made lowercase.
    deletefile = models.BooleanField(
        db_column="DeleteFile"
    )  # Field name made lowercase.
    viewpassword = models.BooleanField(
        db_column="ViewPassword"
    )  # Field name made lowercase.
    updatepassword = models.BooleanField(
        db_column="UpdatePassword"
    )  # Field name made lowercase.
    deletepassword = models.BooleanField(
        db_column="DeletePassword"
    )  # Field name made lowercase.
    createpassword = models.BooleanField(
        db_column="CreatePassword"
    )  # Field name made lowercase.
    viewproductkey = models.BooleanField(
        db_column="ViewProductKey"
    )  # Field name made lowercase.
    updateproductkey = models.BooleanField(
        db_column="UpdateProductKey"
    )  # Field name made lowercase.
    deleteproductkey = models.BooleanField(
        db_column="DeleteProductKey"
    )  # Field name made lowercase.
    createproductkey = models.BooleanField(
        db_column="CreateProductKey"
    )  # Field name made lowercase.
    viewtutorial = models.BooleanField(
        db_column="ViewTutorial"
    )  # Field name made lowercase.
    updatetutorial = models.BooleanField(
        db_column="UpdateTutorial"
    )  # Field name made lowercase.
    deletetutorial = models.BooleanField(
        db_column="DeleteTutorial"
    )  # Field name made lowercase.
    createtutorial = models.BooleanField(
        db_column="CreateTutorial"
    )  # Field name made lowercase.
    departmentlockdown = models.BooleanField(
        db_column="DepartmentLockdown"
    )  # Field name made lowercase.
    viewspam = models.BooleanField(db_column="ViewSpam")  # Field name made lowercase.
    viewspamlogs = models.BooleanField(
        db_column="ViewSpamLogs"
    )  # Field name made lowercase.
    releasespam = models.BooleanField(
        db_column="ReleaseSpam"
    )  # Field name made lowercase.
    addallowblockspam = models.BooleanField(
        db_column="AddAllowBlockSpam"
    )  # Field name made lowercase.
    deleteallowblockspam = models.BooleanField(
        db_column="DeleteAllowBlockSpam"
    )  # Field name made lowercase.
    viewmobiledevices = models.BooleanField(
        db_column="ViewMobileDevices"
    )  # Field name made lowercase.
    updatemobiledevices = models.BooleanField(
        db_column="UpdateMobileDevices"
    )  # Field name made lowercase.
    deletemobiledevices = models.BooleanField(
        db_column="DeleteMobileDevices"
    )  # Field name made lowercase.
    wipemobiledevices = models.BooleanField(
        db_column="WipeMobileDevices"
    )  # Field name made lowercase.
    requiretwofactor = models.BooleanField(
        db_column="RequireTwoFactor"
    )  # Field name made lowercase.
    resellercode = models.TextField(
        db_column="ResellerCode", blank=True, null=True
    )  # Field name made lowercase.
    allowmailboxplanchange = models.BooleanField(
        db_column="AllowMailboxPlanChange"
    )  # Field name made lowercase.
    viewretention = models.BooleanField(
        db_column="ViewRetention"
    )  # Field name made lowercase.
    createretention = models.BooleanField(
        db_column="CreateRetention"
    )  # Field name made lowercase.
    updateretention = models.BooleanField(
        db_column="UpdateRetention"
    )  # Field name made lowercase.
    deleteretention = models.BooleanField(
        db_column="DeleteRetention"
    )  # Field name made lowercase.
    viewpasswordpolicies = models.BooleanField(
        db_column="ViewPasswordPolicies"
    )  # Field name made lowercase.
    createpasswordpolicies = models.BooleanField(
        db_column="CreatePasswordPolicies"
    )  # Field name made lowercase.
    updatepasswordpolicies = models.BooleanField(
        db_column="UpdatePasswordPolicies"
    )  # Field name made lowercase.
    deletepasswordpolicies = models.BooleanField(
        db_column="DeletePasswordPolicies"
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=2048, blank=True, null=True
    )  # Field name made lowercase.
    limitvisibility = models.TextField(
        db_column="LimitVisibility", blank=True, null=True
    )  # Field name made lowercase.
    grantaccess = models.TextField(
        db_column="GrantAccess", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "user_Roles"
